package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (null == inputNumbers) throw new CannotBuildPyramidException();

        ArrayList<Integer> sortedList = new ArrayList();
        try {
            sortedList.addAll(inputNumbers.stream().sorted().collect(Collectors.toList()));
        }catch (Exception ex) {
            throw new CannotBuildPyramidException();
        }

        int count = sortedList.size();

        int rows = geCountOfRows(count);
        if (0 == rows) throw new CannotBuildPyramidException();
        int columns = getCountOfColumn(rows);

        int matrix[][] = new int[rows][columns];

        int pointer = 0;

        for (int i=0; i<rows; i++) {
            int inerted=0;
            for (int j = columns-(rows+i);  j < columns; j=j+2) {
                if (inerted<=i){
                    matrix[i][j] = sortedList.get(pointer);
                    pointer++;
                    inerted++;
                } else break;
            }
        }
        return matrix;
    }

    public int geCountOfRows(int count){
        if (count<=0) return 0;
        double retval = (Math.sqrt(8*count+1) - 1)/2;
        return  0 == Double.compare(retval,(int)retval) ? (int)retval : 0;

    }

    public int getCountOfColumn(int rowCount) {
        return rowCount > 0 ? rowCount*2-1 : 0;
    }




}
