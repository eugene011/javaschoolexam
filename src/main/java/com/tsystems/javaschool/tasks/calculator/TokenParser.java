package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;

public class TokenParser {
    String cache = "";
    ArrayList<Token> retList = new ArrayList();

    public List<Token> splitToToken(String str){
        cache="";
        retList.clear();

        try {
            for (char element : str.toCharArray()) {
                switch (element) {
                    case '+':
                        flush();
                        retList.add(new Token(Type.OPERATOR, OperatorType.PLUS));
                        break;
                    case '-':
                        flush();
                        retList.add(new Token(Type.OPERATOR, OperatorType.MINUS));
                        break;
                    case '*':
                        flush();
                        retList.add(new Token(Type.OPERATOR, OperatorType.MULT));
                        break;
                    case '/':
                        flush();
                        retList.add(new Token(Type.OPERATOR, OperatorType.DIV));
                        break;
                    case '(':
                        flush();
                        retList.add(new Token(Type.LEFT_PARENTHESIS));
                        break;
                    case ')':
                        flush();
                        retList.add(new Token(Type.RIGHT_PARENTHESIS));
                        break;
                    default:
                        cache = cache + String.valueOf(element);
                        break;
                }
            }

            if (!cache.isEmpty()) {
                retList.add(new Token(Double.parseDouble(cache)));
                cache = "";
            }
        } catch (NumberFormatException e) {
            return null;
        }
        return retList;
    }

    public void flush() throws NumberFormatException{
        if (!cache.isEmpty()) {
            retList.add(new Token(Double.parseDouble(cache)));
            cache = "";
        }
    }
}
