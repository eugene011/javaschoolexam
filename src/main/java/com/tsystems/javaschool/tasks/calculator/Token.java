package com.tsystems.javaschool.tasks.calculator;

public class Token {

    private Type type = Type.UNKNOWN;
    private double value;
    private OperatorType operator = OperatorType.NONE;

    public Token(Type type, OperatorType operator) {
        this.type = type;
        this.operator = operator;
    }

    public Token(double value) {
        this.type = Type.NUMBER;
        this.value = value;
    }

    public Token(Type type) {
        this.type = type;
    }

    Type getType() { return type; }
    double getValue() { return value; }
    int getRating() { return  operator.getRating(); }

    Token operate(double a,double b) {
        double result = 0.0;
        switch(operator) {
            case PLUS:
                result = a + b;
                break;
            case MINUS:
                result = a - b;
                break;
            case MULT:
                result = a * b;
                break;
            case DIV:
                result = a / b;
                break;
        }
        return new Token(result);
    }
}