package com.tsystems.javaschool.tasks.calculator;

public enum OperatorType {

    NONE(0),
    PLUS(1),
    MINUS(1),
    DIV(2),
    MULT(2);

    private int rating;

    OperatorType(int rating) {
        this.rating = rating;
    }

    public int getRating() { return rating; }

}
